package com.swiggy.ravi.redis.json;

import java.util.List;

public class PersonJson {

  private String name;
  private List<AddressJson> address;
  private List<String> mobile;
  private List<String> email;

  public PersonJson() {

  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<AddressJson> getAddress() {
    return address;
  }

  public void setAddress(final List<AddressJson> address) {
    this.address = address;
  }

  public List<String> getMobile() {
    return mobile;
  }

  public void setMobile(final List<String> mobile) {
    this.mobile = mobile;
  }

  public List<String> getEmail() {
    return email;
  }

  public void setEmail(final List<String> email) {
    this.email = email;
  }

}
