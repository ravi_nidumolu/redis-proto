package com.swiggy.ravi.redis.json;

import java.util.List;

public class EventJson {

  private final List<String> artists;

  public EventJson(final List<String> artists) {
    this.artists = artists;
  }

  public List<String> getArtists() {
    return artists;
  }

}
