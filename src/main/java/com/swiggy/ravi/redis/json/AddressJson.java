package com.swiggy.ravi.redis.json;

public class AddressJson {

  private String street;
  private int number;

  public AddressJson() {

  }

  public String getStreet() {
    return street;
  }

  public void setStreet(final String street) {
    this.street = street;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(final int number) {
    this.number = number;
  }

}
