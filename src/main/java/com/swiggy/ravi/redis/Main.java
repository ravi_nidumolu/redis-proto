package com.swiggy.ravi.redis;

import com.swiggy.ravi.redis.json.AddressJson;
import com.swiggy.ravi.redis.json.EventJson;
import com.swiggy.ravi.redis.json.PersonJson;
import com.swiggy.ravi.redis.proto.EventProtos;
import com.swiggy.ravi.redis.proto.EventProtos.EventProto;
import com.swiggy.ravi.redis.proto.PeopleProtos.AddressProto;
import com.swiggy.ravi.redis.proto.PeopleProtos.PersonProto;
import com.swiggy.ravi.redis.proto.PeopleProtos.PersonProto.Builder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import redis.clients.jedis.BinaryJedis;
import redis.clients.jedis.Jedis;

public class Main {

  public static void main(final String[] args) throws Exception {
    final BinaryJedis jedis = new BinaryJedis();

    final int num_address = 10;
    final int keys = 100;

    final Long start = System.currentTimeMillis();
    for (int loop = 0; loop < 1000; loop++) {

      // proto(jedis, loop, num_address, keys);

      json(jedis, loop, num_address, keys);
    }
    System.out.println(String.format("Time: %s ms", System.currentTimeMillis() - start));

    jedis.close();
  }


  public static void json(final BinaryJedis jedis, final int loop, final int num_address,
      final int keys)
      throws JsonProcessingException, IOException, JsonParseException, JsonMappingException {
    final ObjectMapper objectMapper = new ObjectMapper();
    for (int i = 0; i < keys; i++) {

      final List<AddressJson> addresses = new LinkedList<>();
      for (int j = 0; j < num_address; j++) {
        final AddressJson addressJson1 = new AddressJson();
        addressJson1.setNumber(j);
        addressJson1.setStreet("Street Number " + j);
        addresses.add(addressJson1);
      }

      final PersonJson personJson = new PersonJson();
      personJson.setName("Json Person Number " + i);
      personJson.setMobile(Arrays.asList(new String[] {"111111" + i, "222222" + i}));
      personJson.setAddress(addresses);
      personJson.setEmail(Arrays.asList(new String[] {"emailPerson" + i + "@somewhere.com",
          "otheremailPerson" + i + "@somewhere.com"}));

      jedis.set(("json_person_" + i + "_" + loop).getBytes(),
          new String(objectMapper.writeValueAsString(personJson)).getBytes());
      objectMapper.readValue(jedis.get(("json_person_" + i + "_" + loop).getBytes()),
          PersonJson.class);
    }
  }


  public static void proto(final BinaryJedis jedis, final int loop, final int num_address,
      final int keys) throws IOException {
    for (int i = 0; i < keys; i++) {
      final Builder personBuilder = PersonProto.newBuilder();

      for (int j = 0; j < num_address; j++) {
        personBuilder.addAddress(
            AddressProto.newBuilder().setStreet("Street Number " + j).setNumber(j).build());
      }

      final PersonProto personProto =
          personBuilder.setName("Proto Person Number " + i).addMobile("111111" + i)
              .addMobile("222222" + i).addEmail("emailPerson" + i + "@somewhere.com")
              .addEmail("otheremailPerson" + i + "@somewhere.com").build();
      jedis.set(("proto_person_" + i + "_" + loop).getBytes(), personProto.toByteArray());

      final InputStream cacheProtoStream =
          new ByteArrayInputStream(jedis.get(("proto_person_" + i + "_" + loop).getBytes()));
      PersonProto.parseFrom(cacheProtoStream);
    }
  }


  public static void event(final Jedis jedis) throws Exception {
    final EventProto proto = EventProtos.EventProto.newBuilder().addArtist("1").addArtist("2")
        .addArtist("3").addArtist("4").build();
    jedis.set("proto", new String(proto.toByteArray()));

    final InputStream cachedStream = new ByteArrayInputStream(jedis.get("proto").getBytes());
    final EventProto cachedProto = EventProto.parseFrom(cachedStream);
    System.out.println(cachedProto.toString());

    final ObjectMapper objectMapper = new ObjectMapper();
    jedis.set("json", objectMapper
        .writeValueAsString(new EventJson(Arrays.asList(new String[] {"1", "2", "3"}))));
    System.out.println(jedis.get("json"));

  }

  public static final long MEGABYTE = 1024L * 1024L;

  public static long bytesToMegabytes(final long bytes) {
    return bytes / MEGABYTE;
  }

}
